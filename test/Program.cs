﻿using System;
using Eccentrade;
using System.Threading.Tasks;
using System.Threading;

namespace test
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			try {
				Eccentrade.Client client = new Client ();

				var loginResult = client.Login (new LoginRequest {
					Email = "apitest@eccentrade.com",
					Password = "apitest123",
					AppId = "0OSokGwsOt0ZhuYJVJkgeFn5fjfk74g3vxUao4dBJnI"
				});
				if (loginResult.Exception != null)
					throw loginResult.Exception;
				
				client.Token = loginResult.Token;
				client.RefreshToken = loginResult.RefreshToken;

				var searchResult = client.Search ("s");
				var company = client.GetCompany (searchResult.Results[0].Guid);
			}
			catch (Eccentrade.ApiException ex) {
				Console.Error.WriteLine (ex.ToString ());
			}
		}
	}
}
