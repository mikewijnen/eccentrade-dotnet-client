﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;

namespace Eccentrade
{
	public partial class Client
	{
		public LoginResponse Login(string appId, string email, string password) {
			var data = new LoginRequest { AppId = appId, Email = email, Password = password };
			return Login (data);
		}
		public LoginResponse Login(LoginRequest request) {
			LoginResponse response = Post<LoginResponse> ("/auth/login", request).Result;
			if (response.Exception != null)
				throw new ApiException(response);
			return response;
		}
	}
}

