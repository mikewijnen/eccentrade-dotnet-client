﻿using System;

namespace Eccentrade
{
	public class ApiException : Exception
	{
		public ApiException (IClientResponse response)
			: base(response.Exception.Message, response.Exception)
		{
			this.Response = response;
		}

		public IClientResponse Response { get; private set; }
	}
}

