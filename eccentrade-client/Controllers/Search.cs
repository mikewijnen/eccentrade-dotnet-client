﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Eccentrade
{
	public partial class Client
	{
		public SearchResponse Search(string query) {
			var data = new SearchRequest { Query = query };
			return Search (data);
		}
		public SearchResponse Search(SearchRequest request) {
			SearchResponse response = Get<SearchResponse> ("/search/companies", request).Result;
			if (response.Exception != null)
				throw new ApiException(response);
			return response;
		}
	}
}

