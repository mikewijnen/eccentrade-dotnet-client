﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Eccentrade
{
	public partial class Client
	{
		public CompanyResponse GetCompany(string guid) {
			var data = new CompanyRequest { Guid = guid };
			return GetCompany (data);
		}
		public CompanyResponse GetCompany(CompanyRequest request) {
			CompanyResponse response = Get<CompanyResponse> (string.Format ("/companies/{0}", request.Guid)).Result;
			if (response.Exception != null)
				throw new ApiException(response);
			return response;
		}
	}
}

