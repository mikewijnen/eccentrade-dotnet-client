﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;

namespace Eccentrade
{
	public partial class Client
	{
		public Client ()
			: this("http://localhost:8080/") { }

		public Client (string address) {
			this.address = address;
		}

		private string address;

		private HttpWebRequest GetRequest(string path) {
			Uri uri = new Uri (new Uri(this.address), path);
			Console.WriteLine ("GetRequest({0})", uri);
			return HttpWebRequest.Create (uri) as HttpWebRequest;
		}

		public string Token { get; set; }
		public string RefreshToken { get; set; }
	}
}

