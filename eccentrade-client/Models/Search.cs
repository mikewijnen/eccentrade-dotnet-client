﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eccentrade
{
	public class SearchRequest : ClientRequest {
		[DataMember(Name = "q", EmitDefaultValue = false)]
		public string Query { get; set; }
		[DataMember(Name = "active", EmitDefaultValue = false)]
		public bool? Active { get; set; }
		[DataMember(Name = "activities", EmitDefaultValue = false)]
		public List<string> Activities { get; set; }
		[DataMember(Name = "branch", EmitDefaultValue = false)]
		public List<string> Branches { get; set; }
		[DataMember(Name = "certifications", EmitDefaultValue = false)]
		public List<string> Certifications { get; set; }
		[DataMember(Name = "zipcode", EmitDefaultValue = false)]
		public string Zipcode { get; set; }
		[DataMember(Name = "country", EmitDefaultValue = false)]
		public string Country { get; set; }
		[DataMember(Name = "skip", EmitDefaultValue = false)]
		public string PageOffset { get; set; }
		[DataMember(Name = "limit", EmitDefaultValue = false)]
		public string PageSize { get; set; }
	}

	[Serializable]
	[DataContract]
	public class SearchResponse : ClientResponse {
		[DataMember(Name = "results", EmitDefaultValue = false)]
		public List<Company> Results { get; set; }
		[DataMember(Name = "total", EmitDefaultValue = false)]
		public long Count { get; set; }
		[DataMember(Name = "time", EmitDefaultValue = false)]
		public long Duration { get; set; }
	}
}

