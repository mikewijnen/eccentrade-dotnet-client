﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eccentrade
{
	public interface IClientRequest {

	}

	public interface IClientResponse {
		HttpStatusCode Status { get; set; }
		Exception Exception { get; set; }
		Dictionary<string, object> Properties { get; set; }
	}

	[DataContract]
	public class ClientRequest : IClientRequest {
	}

	[DataContract]
	public class ClientResponse : IClientResponse {
		public HttpStatusCode Status { get; set; }
		public Exception Exception { get; set; }
		public Dictionary<string, object> Properties { get; set; }
	}
}

