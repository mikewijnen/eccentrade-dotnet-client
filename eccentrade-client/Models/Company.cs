﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eccentrade
{
	[DataContract]
	public class Company : ClientResponse {
		// The 16 character Global Unique ID number of the company.
		[DataMember(Name = "guid")]
		public string Guid { get; set; }

		public class IdCollection {
			[DataMember(Name = "local")]
			public string Local { get; set; }
			[DataMember(Name = "establishment")]
			public string Establishment { get; set; }
			[DataMember(Name = "rsin")]
			public string Rsin { get; set; }
		};

		[DataMember(Name = "ids")]
		public IdCollection Ids { get; set; }

		[DataMember(Name = "name")] // The primary company trade name.
		public string Name { get; set; }

		[DataMember(Name = "tradeNames")] // The full list of trade names for this company.
		public string TradeNames { get; set; }

		[DataMember(Name = "legalName")] // The official legal name of the company.
		public string LegalName { get; set; }

		[DataMember(Name = "legalForm")]
		public int LegalForm { get; set; }

		/*activities: Joi.array().description('A list of company activities. The classification used depends on the country.').example(['Creation of rocket powered products']),
		activitiesStandard: Joi.string().description('The industry classification standard that the activity codes reflect.').example('NACE'),
		address: Joi.object({
			line1: Joi.string().required().description('Address line 1.').example('15c Desert Road'),
			line2: Joi.string().description('Address line 2.').example('Austin'),
			line3: Joi.string().description('Address line 3.').example(' Texas'),
			street: Joi.string().description('Street (deprecated)').example('Desert Road'),
			number: Joi.string().description('Number (deprecated)').example('15'),
			affix: Joi.string().description('Affix (deprecated)').example('c'),
			city: Joi.string().required().description('City / Town (deprecated)').example('c'),
			zipcode: Joi.string().required().description('Zip / Postal code.').example('12345'),
			region: Joi.string().description('Region.').example(' Travis County'),
			country: Joi.string().required().description('Country.').example('United States'),
		}),
		mailAddress: Joi.object({
			line1: Joi.string().description('Mail Address line 1.').example('P.O Box 1234'),
			line2: Joi.string().description('Address line 2.').example('Austin'),
			line3: Joi.string().description('Address line 3.').example(' Texas'),
			street: Joi.string().description('Street (deprecated)').example('Desert Road'),
			number: Joi.string().description('Number (deprecated)').example('15'),
			affix: Joi.string().description('Affix (deprecated)').example('c'),
			city: Joi.string().description('City / Town (deprecated)').example('c'),
			zipcode: Joi.string().description('Zip / Postal code.').example('12345'),
			region: Joi.string().description('Region.').example(' Travis County'),
			country: Joi.string().description('Country.').example('United States'),
		}),
		location: Joi.object({
			type: Joi.string().allow('Point').default('Point'),
			coordinates: Joi.array().length(2).items(Joi.number().min(-180).max(180).required()).description('The latitude longitude coordinates conform GeoJSON (RFC 7946) specifications.').required(),
		}),
		dates: Joi.object({
			memorandum: Joi.date().description('Founding date.'),
			registration: Joi.date().description('Registration date.'),
			establishment: Joi.date().description('Date on which the company established itself on it\'s current location.'),
			deregistration: Joi.date().description('Date on which the company deregistered.'),
			continuation: Joi.date().description('Date on which the company continued it\`s operations.'),
		}),
		phone: Joi.string().max(16).description('Phone number').example('+1 123 4567 8901'),
		mobile: Joi.string().max(16).description('Phone number').example('+1 123 4567 8901'),
		emails: Joi.array().items(Joi.object({
			address: Joi.string().description('The email address').example('info@acmeinc.biz'),
			verified: Joi.boolean().default(false).description('True when the address has been verified.'),
			description: Joi.string().description('An optional description.'),
		})).description('Email addresses'),
		url: Joi.string().description('Website').example('www.acmeinc.biz'),
		owner: Joi.object({
			titles: Joi.array().description('Titles of the owner.').example(['MSc']),
			initials: Joi.array().description('Initials .').example(['R.']),
			lastName: Joi.string().description('Last name.').example('Runner'),
			firstName: Joi.string().description('First name.').example('Road'),
		}),
		employees: Joi.object({
			number: Joi.number().description('Number of employees working at this company.').example(3),
			exactNumber: Joi.number().description('Exact number of employees working at this company expressed in FTE (Full Time Employee) units.').example(3),
			refDate: Joi.date().description('Date on which employee numbers were last updated by the company.'),
		}),
		registrationReason: Joi.string().description('(Dutch companies only) The reason the company registered.').example('14'),
		deregistrationReason: Joi.string().description('(Dutch companies only) The reason the company seized to exist.').example('27'),
		flags: Joi.object({
			branch: Joi.bool().description('True when the company is branch office, false if it is the head office.').example(true),
			active: Joi.bool().description('True when the company is currently still active.').example(true),
			trading: Joi.bool().description('True when the company is economically active (i.e. no holdings).').example(true),
			importing: Joi.bool().description('True when the company is known to export goods or services.').example(false),
			exporting: Joi.bool().description('True when the company is known to import goods or services.').example(true),
			nonMailing: Joi.bool().description('True when the company does not appreciate to receive unwanted mailings.').example(true),
		}),
		finance: Joi.object({
			latestAccounts: Joi.date().description('Date on which the latest accounts were filed.'),
			consolidatedAccounts: Joi.bool().description('True when the numbers are based on consolidated accounts.'),
			creditWorthy: Joi.bool().description('True when the company is considered to be credit worthy.'),
			creditScore: Joi.number().description('The latest credit score.').example(91),
			creditLimit: Joi.number().description('The advised credit limit.').example(1000000),
			latestTurnover: Joi.number().description('The latest turnover figure.').example(1000000),
			shareCapital: Joi.number().description('The total amount of share capital').example(3000000),
		}),
		certifications: Joi.array().items(Joi.object({
			name: Joi.string().description('Certification name').example('ISO 14001'),
			institute: Joi.string().description('Certification institute').example('Peek Valley'),
			releaseYear: Joi.number().min(1970).max(2100).description('Date on which the certification was released.'),
			validUntil: Joi.date().description('Date on which the certification expires.'),
		})),
		changes: Joi.object({
			history: Joi.array().items(Joi.object({
				change: Joi.object({
					previousValue: Joi.string().description('Previous value'),
					newValue: Joi.string().description('New value'),
					code: Joi.string().description('The name of the changed field'),
					date: Joi.date().description('The date of the change'),
				})
			})),
			oldestChange: Joi.date().description('Date of the first change.'),
			latestChange: Joi.date().description('Date of latest change.'),
		}),*/
	}

	[DataContract]
	public class CompanyRequest : Company {
		[DataMember(Name = "guid")]
		public string Guid { get; set; }
	}

	[DataContract]
	public class CompanyResponse : Company {
	}
}

