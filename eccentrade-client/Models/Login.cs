﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eccentrade
{
	[DataContract]
	public class LoginRequest : ClientRequest {
		[DataMember(Name = "appId", EmitDefaultValue = false)]
		public string AppId { get; set; }
		[DataMember(Name = "email", EmitDefaultValue = false)]
		public string Email { get; set; }
		[DataMember(Name = "username", EmitDefaultValue = false)]
		public string Username { get; set; }
		[DataMember(Name = "password", EmitDefaultValue = false)]
		public string Password { get; set; }
	}

	[DataContract]
	public class LoginResponse : ClientResponse {
		[DataMember(Name = "token", EmitDefaultValue = false)]
		public string Token { get; set; }
		[DataMember(Name = "expiresIn", EmitDefaultValue = false)]
		public int ExpiresIn { get; set; }
		[DataMember(Name = "refreshToken", EmitDefaultValue = false)]
		public string RefreshToken { get; set; }
		[DataMember(Name = "scope", EmitDefaultValue = false)]
		public List<string> Scope { get; set; }
	}
}

