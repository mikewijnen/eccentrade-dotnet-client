﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Runtime.Serialization;

namespace Eccentrade
{
	public partial class Client
	{
		public async Task<ResponseType> Get<ResponseType>(string path) where ResponseType : ClientResponse, new() {
			return Get<ResponseType> (path, null).Result;
		}
		public async Task<ResponseType> Get<ResponseType>(string path, ClientRequest request) where ResponseType : ClientResponse, new() {
			try {
				if (request != null) {
					var properties = request.GetType().GetProperties()
						.Where(prop => prop.IsDefined(typeof(DataMemberAttribute), false));

					string query = "";
					foreach(var property in properties) {
						DataMemberAttribute attribute = property.GetCustomAttributes(typeof(DataMemberAttribute), false).First() as DataMemberAttribute;
						string name = attribute.Name;
						object value = property.GetValue(request);
						if (value == null)
							continue;
						string svalue;
						if (value is DateTime)
							svalue = ((DateTime)value).ToString("s");
						else if (value is bool)
							svalue = value.ToString().ToLower();
						else
							svalue = value.ToString();
						
						if (!string.IsNullOrEmpty(query))
							query += "&";
						query += string.Format("{0}={1}", name, Uri.EscapeDataString(svalue));
					}
					if (!string.IsNullOrEmpty(query))
						query = "?" + query;

					path += query;
				}

				HttpWebRequest webRequest = GetRequest (path);

				webRequest.Method = "GET";
				webRequest.ContentType = "application/json";

				if (!string.IsNullOrEmpty(this.Token)) {
					webRequest.Headers["Authorization"] = string.Format("Bearer {0}", this.Token);
				}

				HttpWebResponse webResponse = await webRequest.GetResponseAsync () as HttpWebResponse;
				ResponseType responseObject = ConvertResponse<ResponseType>(webResponse);
				return responseObject;
			}
			catch (WebException ex) {
				return new ResponseType {
					Exception = ex,
					Status = (ex.Response as HttpWebResponse).StatusCode
				};
			}
			catch (Exception ex) {
				return new ResponseType {
					Exception = ex,
					Status = HttpStatusCode.InternalServerError
				};
			}
		}

		public async Task<ResponseType> Post<ResponseType>(string path, ClientRequest request) where ResponseType : ClientResponse, new() {
			try {
				HttpWebRequest webRequest = GetRequest (path);

				string data;
				using (MemoryStream ms = new MemoryStream()) {
					DataContractJsonSerializer sz = new DataContractJsonSerializer (request.GetType());
					sz.WriteObject(ms, request);
					ms.Flush();

					ms.Seek(0, SeekOrigin.Begin);
					using (StreamReader reader = new StreamReader(ms))
						data = reader.ReadToEnd();
				}

				webRequest.Method = "POST";
				webRequest.ContentType = "application/json";
				webRequest.ContentLength = data.Length;

				using (StreamWriter writer = new StreamWriter (webRequest.GetRequestStream ()))
					writer.Write (data);

				HttpWebResponse webResponse = await webRequest.GetResponseAsync () as HttpWebResponse;
				ResponseType responseObject = ConvertResponse<ResponseType>(webResponse);
				return (responseObject);
			}
			catch (WebException ex) {
				return (new ResponseType {
					Exception = ex,
					Status = (ex.Response as HttpWebResponse).StatusCode
				});
			}
			catch (Exception ex) {
				return (new ResponseType {
					Exception = ex,
					Status = HttpStatusCode.InternalServerError
				});
			}
		}

		private ResponseType ConvertResponse<ResponseType>(HttpWebResponse response) where ResponseType : ClientResponse {
			using (StreamReader reader = new StreamReader (response.GetResponseStream ())) {
				ResponseType responseObject;
				string content = reader.ReadToEnd ();
				using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(content))) {
					DataContractJsonSerializer sz = new DataContractJsonSerializer (typeof(ResponseType));
					responseObject = sz.ReadObject(ms) as ResponseType;
				}
				responseObject.Status = response.StatusCode;
				responseObject.Properties = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(content);
				return responseObject;
			}
		}
	}
}

